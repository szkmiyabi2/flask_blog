# flask_blog

## Windows環境での留意点メモ

pythonのパスが通っていない/通っているが認識しない場合は下記の方法で実行する。

### py コマンドを使って対処
```
>py server.py
>py manage.py init_db
```
(参考URL)[https://gammasoft.jp/blog/resolve-not-to-run-python/]

### flaskコマンドを使う（DBミグレーションは不可能）
```
> set FLASK_APP=server.py
> flask run
```

## flask以外に必要なもの

### Flask-SQLAlchemy

```
>pip install Flask-SQLAlchemy
```
VSCodeのターミナルでFlaskを実行するとプロセスが残っているらしくインストール失敗した。

Win再起動したら問題なく入った。

### Flask-Script
```
>pip install Flask-Script
```

## CentOS7環境へのデプロイ対応
sqlite3は書き込み権限エラーが発生し対処方法が見つからずMariaDBに代替することにした。

Windows環境側にもMariaDBを導入した。

### 環境変数
```
[新規]
名前：MARIA_HOME
値：C:\Program Files\MariaDB 10.4

Path
%MARIA_HOME%\bin
```

### パスが通っているか確認
```
>mysql --version
mysql  Ver 15.1 Distrib 10.4.11-MariaDB, ....
```

### mysql-connector
```
>pip install mysql-connector-python
```

### 学習保留内容
```
Blueprint
unittest
```
